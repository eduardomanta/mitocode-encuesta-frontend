import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LoginDTO } from '../dto/login-dto';
import { Respuesta } from '../models/respuesta';
import { HOST_BACKEND, PARAM_USUARIO, TOKEN_NAME } from '../shared/constants';
import { PasswordChangeDTO } from '../dto/password-change-dto';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private $userSubject:BehaviorSubject<any>;
  private $currentUser:Observable<any>;
  private urlLogin:string=`${HOST_BACKEND}/api/security/login`;
  private urlResetFirst:string=`${HOST_BACKEND}/api/security/first-reset-password`;
  private urlCheckToken:string=`${HOST_BACKEND}/api/security/token`;
  private urlSignOut:string=`${HOST_BACKEND}/api/security/signout`;

  constructor(private http:HttpClient) { 
    this.$userSubject= new BehaviorSubject<any>(JSON.parse(localStorage.getItem(PARAM_USUARIO)));
    this.$currentUser=this.$userSubject.asObservable();
  }

  getUserObservable(){
    return this.$currentUser;
  }

  setObservableUser(user:any){
    this.$userSubject.next(user);
  }

  getUser(){
    return this.$userSubject.value;
  }
  isRoleAdmin(){
    let usuario = JSON.parse(localStorage.getItem(PARAM_USUARIO));
    let rpta = false;
    if(usuario != null && usuario.authorities !== null) {
      usuario.authorities.forEach(element => {
        if(element.authority == "ROLE_ADMIN" || element.authority == "ROLE_ADMINISTRADOR"){   
          rpta = true;
        }
      });
    }
    return rpta;
  }

  login(loginDTO:LoginDTO):Observable<Respuesta>{
    return this.http.post<Respuesta>(this.urlLogin,loginDTO);
  };
  resetFirstPassword(passwordChangeDTO:PasswordChangeDTO):Observable<Respuesta>{
    return this.http.post<Respuesta>(this.urlResetFirst,passwordChangeDTO);
  };

  checkToken(){
    return this.http.post<any>(this.urlCheckToken,null);
  };

  signOut():Observable<Respuesta>{
    return this.http.post<Respuesta>(this.urlSignOut,{token:localStorage.getItem(TOKEN_NAME)});
  }
}