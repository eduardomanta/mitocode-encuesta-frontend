import { Component, OnInit } from '@angular/core';
import { Encuesta } from 'src/app/models/encuesta';
import { EncuestaService } from 'src/app/services/encuesta.service';

const ENCUESTA_COLUMNS: string[] = ['id', 'nombres', 'apellidos', 'edad', 'curso'];
@Component({
  selector: 'app-encuesta-lista',
  templateUrl: './encuesta-lista.component.html',
  styleUrls: ['./encuesta-lista.component.css']
})
export class EncuestaListaComponent implements OnInit {
  lstEncuesta:Encuesta[];
  displayedColumns=ENCUESTA_COLUMNS;
  constructor(private encuestaService:EncuestaService) { 
    this.lstEncuesta=new Array();
  }

  ngOnInit() {
    this.encuestaService.list().subscribe((data)=>{
      this.lstEncuesta=data;
    },(err)=>{
      console.log(err);
    })
  }
}
