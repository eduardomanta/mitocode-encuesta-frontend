import { Component, OnInit } from '@angular/core';
import { PasswordChangeDTO } from 'src/app/dto/password-change-dto';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SESION_NAME, TOKEN_NAME, PARAM_USUARIO } from 'src/app/shared/constants';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-first-password',
  templateUrl: './login-first-password.component.html',
  styleUrls: ['./login-first-password.component.css']
})
export class LoginFirstPasswordComponent implements OnInit {
  passwordChangeDTO: PasswordChangeDTO;
  constructor(private authentication: AuthenticationService,private router:Router,private route: ActivatedRoute) {
    this.passwordChangeDTO = new PasswordChangeDTO;
    setTimeout(() => {
      this.passwordChangeDTO.password = "";
    }, 100);
  }

  ngOnInit() {
    this.passwordChangeDTO.session=localStorage.getItem(SESION_NAME);
    this.route.queryParamMap.subscribe((data)=>{
      this.passwordChangeDTO.username=data.get('username');
    })
  }

  onChangePassword() {
    this.authentication.resetFirstPassword(this.passwordChangeDTO).subscribe((data) => {
      if (data.status === 'OK') {
        localStorage.removeItem(SESION_NAME)
        localStorage.setItem(TOKEN_NAME, data.accessToken);
        this.authentication.checkToken().subscribe((response: any) => {
          localStorage.setItem(PARAM_USUARIO, JSON.stringify(response.body));
          this.authentication.setObservableUser(response.body);
          if (this.authentication.isRoleAdmin()) {
            this.router.navigate(["admin/encuesta-lista"]);
          } else {
            this.router.navigate(["encuesta"]);
          }
        }, (err) => {
          console.log(err);
          localStorage.removeItem(TOKEN_NAME);
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

}
