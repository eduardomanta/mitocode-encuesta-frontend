import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { SESION_NAME } from '../shared/constants';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let currentUser = this.authenticationService.getUser();
        let isAdmin = this.authenticationService.isRoleAdmin();
        
        if (currentUser) {
            if (!isAdmin) {
                switch (state.url) {
                    case '/admin/encuesta-lista':
                        this.router.navigate(['encuesta']);
                        return false;
                }
            }else{
                switch (state.url) {
                    case '/encuesta':
                        this.router.navigate(['encuesta-lista']);
                        return false;
                }
            }
            return true;
        }
        if(state.url.indexOf('/login-first-reset')!==-1){
            if (localStorage.getItem(SESION_NAME)===null) {
                this.router.navigate(['']);
                return false;
            }else{
                return true;
            }
        }else{
            this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
            return false;
        }
    }
}
