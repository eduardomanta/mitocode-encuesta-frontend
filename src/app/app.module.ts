import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouteModule } from './modules/route/route.module';
import { MaterialModule } from './modules/material/material.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SecurityComponent } from './components/security/security.component';
import { AlertComponent } from './components/alert/alert.component';
import { TokenInterceptor } from './interceptors/token-interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { EncuestaListaComponent } from './components/encuesta-lista/encuesta-lista.component';
import { LoginFirstPasswordComponent } from './components/login-first-password/login-first-password.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EncuestaComponent,
    HomeComponent,
    SecurityComponent,
    AlertComponent,
    EncuestaListaComponent,
    LoginFirstPasswordComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouteModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [    {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})

export class AppModule { }
