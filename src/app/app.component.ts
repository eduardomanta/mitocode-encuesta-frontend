import { Component, OnInit } from '@angular/core';
import { Router, RouterStateSnapshot } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import { AuthenticationService } from './services/authentication.service';
import { PARAM_USUARIO, TOKEN_NAME } from './shared/constants';
import { Respuesta } from './models/respuesta';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title = 'Java AWS';
  public $user:Observable<any>;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private authenticationService:AuthenticationService,private router:Router) {
    iconRegistry.addSvgIcon('account',sanitizer.bypassSecurityTrustResourceUrl('assets/icon/account.svg'));
  }
  ngOnInit(){
    this.$user=this.authenticationService.getUserObservable();
  }
  onLogout(){
    this.authenticationService.signOut().subscribe((data:Respuesta)=>{
        localStorage.removeItem(PARAM_USUARIO);
        localStorage.removeItem(TOKEN_NAME);
        this.authenticationService.setObservableUser(null);
        this.router.navigate(['login']);
    },(err)=>{
      localStorage.removeItem(PARAM_USUARIO);
      localStorage.removeItem(TOKEN_NAME);
      this.authenticationService.setObservableUser(null);
      this.router.navigate(['login']);
      console.log(err);
    });
  }
}
